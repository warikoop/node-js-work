var bodyParser = require('body-parser'),
clientCertificateAuth = require('client-certificate-auth');
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use((req, res, next) =>
{

  // console.log(req.secure);
  // console.log(req.header('x-forwarded-proto') )
  next();

});

app.set('view engine','ejs')
app.use(express.static(__dirname +"/public"))
var routes = require('./api/routes/routes'); //importing route
routes(app);
app.listen(port);
console.log('Power BI snapshot service started on: ' + port);
