var memCache = require('memory-cache')
exports.apiCache = function(){
    
    this.cache = this.cache||new memCache.Cache();
    
    return ({get: (key) => this.cache.get(key),
        set: (key,value) =>{this.cache.put(key,value)},
        remove: (key) =>{this.cache.del(key)},
        has:(key) => this.keys.includes(key)
    })};
