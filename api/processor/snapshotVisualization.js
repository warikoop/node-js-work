import puppeteer from 'puppeteer';

export default function generateSnapshot(vizSnapshotConfig)
{
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    page.setViewport({ height: vizSnapshotConfig.viewPort.height, width: vizSnapshotConfig.viewPort.width, deviceScaleFactor: 2 });
    await page.goto('https://ocpinsights.azurewebsites.net/');
}