'use strict';
module.exports = function(app) {
  var snapshots = require('../controllers/snapshotController');
  var reports = require('../controllers/reportController');
  
  app.route('/snapshot/request').post(snapshots.create_snapshot_request)
  app.route('/report').get(reports.render_report)

}