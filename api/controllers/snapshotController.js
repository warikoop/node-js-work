const puppeteer = require('puppeteer');
const uuid = require('uuid/v4');
exports.create_snapshot_request = async function(req, res)
{
    var image;
    
    try{
        let browserConfig = {executablePath:'node_modules/puppeteer/.local-chromium/win64-575458/chrome-win32/chrome.exe'};
        const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
        const page = await browser.newPage();
        page.setViewport({ height: 600, width: 800, deviceScaleFactor: 2 });
        await page.goto('http://localhost:3000/report',{timeout:3000});
        await page.waitForNavigation({ waitUntil: "networkidle2", timeout: 10000 }).then(function () { }).catch(function(){});
        image = uuid()+".png";
        await page.screenshot({ path: image, fullPage: true });
        await browser.close();
    }
    catch(e){
        res.json(e);
    }
    res.sendFile(image,{root: __dirname})
}

